# Traefik

## Setup

### 1. Environment erstellen:

Wir kopieren uns das `.env.template` nach `.env` um dann die Konfigurationsvariablen zu vergeben.

```
cp .env.template .env

nano .env
```

Nun die Variablen anhand der Dokumentation und Kommentare anpassen.

### 2. Netzwerk erstellen

Wir führen nun `./create-network.bash` aus, welches uns das globale Traefik Netzwerk erstellt.

```
./create-network.bash
```


### 3. Docker

Jetzt können wir unsere Docker Umgebung starten.

```
docker-compose pull
docker-compose up -d
```

### 4. Fertig

Nun können wir unsere Docker Services welche mit Traefik Labels ausgestattet sind aufrufen.

Oder Traefik selbst: https://traefik.localhost/traefik
Oder whoami: https://traefik.localhost/whoami