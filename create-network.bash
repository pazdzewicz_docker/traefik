#!/bin/bash
SCRIPT_PATH="$(cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)"
LOCK_FILE="${SCRIPT_PATH}/network.lock"
DOCKER="$(which docker)"

if [ -f "${LOCK_FILE}" ]
then
  echo "${LOCK_FILE} exists."
  exit 1
fi

${DOCKER} network create traefik > ${LOCK_FILE}